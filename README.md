## Getting started

How you integrate Eyes with Bitbucket depends on various factors which effect the communication of status information between Eyes and Bitbucket. 
There are three typical scenarios, depending on whether you use the Bitbucket Cloud or the Bitbucket Server (Data Center) 
and in the case of the server how the Eyes server interacts with the Bitbucket server:

You use the Bitbucket Cloud - see section How to configure the Eyes Bitbucket Cloud integration

You have a Bitbucket Server and the Eyes server can directly interact with Bitbucket - see How to configure the Eyes Bitbucket Server or Data Center integration

You have a Bitbucket Server that is on a secure network which the Eyes server can't access, but the Applitools supplied proxy service can execute and can act as a bridge between the two systems- this is described in the article [Using the Applitools Bitbucket proxy service](https://applitools.com/docs/topics/integrations/bitbucketproxyserver.html)

## How to configure the Eyes Bitbucket Cloud integration
Navigate to your Admin/Team page:

1. Use the Page navigator to display the Admin page. You will only see this page in the Page navigator if you have Admin privileges.
2. Select the Teams  tile.
3. Click on the row of the team you want to configure to open the team configuration page.
4. Scroll down the page to find the Bitbucket section
5. Since your organization uses an Eyes Cloud server, you don't need to set the server - just make sure that it is set to the Bitbucket cloud URL (bitbucket.org). 
6. Click the Manage repositories button. This will open up a dialog which requests that you login to bitbucket. Do this by clicking the link displayed in that dialog The login you use has to have authorization rights to add and remove webhooks for the repositories that you want Eyes to be integrated with.

## How to configure the Eyes Bitbucket Server or Data Center integration
Navigate to your Admin/Team page:

1. Use the Page navigator to display the Admin page. (You will only see this page in the Page navigator if you have Admin privileges).
2. Select the Teams  tile.
3. Click on the row of the team you want to configure to open the team configuration page.
4. Scroll down the page to find the Bitbucket section
5. Add a Bitbucket server to integrate with. In the server selection menu, look for, and select your Bitbucket server. If it is not on the list, then click the Add button. This will open up a wizard which will guide you through the rest of the process; you will need the domain name of your Bitbucket Server or Data Center to start this process.
6. if the Bitbucket Server/Data Center are deployed behind a company firewall you need to whitelist inbound HTTPS traffic from the Eyes server to your Bitbucket server in your company firewall. The Eyes server will be https://eyes.applitools.com if you use the Eyes public cloud or https://myOrg.applitools.com with your organizations name if you have a dedicated cloud or on-prem Eyes system. If you can't whitelist the Eyes server then you should use the Applitools proxy server as described in the article [Using the Applitools Bitbucket proxy service](https://applitools.com/docs/topics/integrations/bitbucketproxyserver.html) instead of the configuration procedure described in this section.
7. Configure your team's repositories. Click the Manage repositories button. This will open up a wizard which will guide you through the rest of the process. As part of the process you need to select an authentication method. You can choose from using a Personal Access Token (PAT) or Basic Authorization.
8. If using a Personal Access Token, then when creating the PAT you must set Admin level permissions for accessing the repositories. For information on creating a PAT see [generating Personal access tokens.](https://confluence.atlassian.com/bitbucketserver/personal-access-tokens-939515499.html)
9. If using Basic Authorization to access the server, then you need only provide the User name and Password credential pair for accessing the server.

# How to configure Bitbucket to run Eyes tests
In the Bitbucket configuration file bitbucket-pipeline.yml, add the following lines:

```
script:
    - export APPLITOOLS_API_KEY="Your Api Key"
    - echo $APPLITOOLS_API_KEY
    - export APPLITOOLS_BATCH_ID=$BITBUCKET_COMMIT
    - echo $APPLITOOLS_BATCH_ID
```

- $BITBUCKET_COMMIT is the default environment variable for the Bitbucket commit sha, this is used with Bitbucket's CI/CD.
- Replace the string YourAPiKey with your API key. To get the value of your API key, see How to obtain your API key.

# Add a CI/CD variable to a project
You can add, edit, or remove variables at the account, repository, and deployment environment levels. If you use the same name as an existing variable, you can override it. The order of overrides is Deployment > Repository > Account > Default variables. Each deployment environment is independent so you can use the same variable name with different values for each environment.
Before you begin bear in mind that:
- Names can only contain ASCII letters, digits and underscores
- Names are case-sensitive
- Names can't start with a digit

Variables defined by the shell should not be used. You can find them by using a step with the command printenv.
Do not configure a pipeline variable with the name PATH or you might break all the pipeline steps. 
This happens because the shell uses PATH to find commands, so if you replace its usual list of locations then commands like docker won't work any more

You then cann add your APPLITOOLS_API_KEY as an environment variable.
In the Bitbucket configuration file:
Please note that the variable is masked, you will not see your key when echo-ing it.

```
script:
    - echo $APPLITOOLS_API_KEY
    - export APPLITOOLS_BATCH_ID=$BITBUCKET_COMMIT
    - echo $APPLITOOLS_BATCH_ID
```

# Bitbucket CI Configuration File "bitbucket-pipeline.yml"

Example:

```
image: maven:3.6.3

pipelines:
  default:
    - parallel:
      - step:
          name: Build and Test
          caches:
            - maven
          script:
            - echo $APPLITOOLS_API_KEY
            - export APPLITOOLS_BATCH_ID=$BITBUCKET_COMMIT
            - echo $APPLITOOLS_BATCH_ID
            - mvn -B verify --file pom.xml
              
          after-script:
              # Collect checkstyle results, if any, and convert to Bitbucket Code Insights.
            - pipe: atlassian/checkstyle-report:0.3.0
      - step:
          name: Security Scan
          script:
            # Run a security scan for sensitive data.
            # See more security tools at https://bitbucket.org/product/features/pipelines/integrations?&category=security
            - pipe: atlassian/git-secrets-scan:0.5.1
```

You can read more information about Bitbucket yml file [here](https://support.atlassian.com/bitbucket-cloud/docs/configure-bitbucket-pipelinesyml/)

# Working with Bitbucket
Once your system is set up as described above, you use your CI and Bitbucket in the usual way. 
A CI job is normally triggered by a push to your repository. This, in turn, will then run your visual UI tests.

The Eyes Bitbucket integration is based on the Bitbucket pull request. 
When there is an open pull request, Eyes adds the following functionality to your Bitbucket workflow:

- When your CI initiates a build that includes an Eyes test, Eyes detects that the run is associated with a Bitbucket pull request and will search for a baseline to use as a reference. Eyes will check for a matching baseline in the following order:
  - On a branch that corresponds to the repository name and source branch defined by the pull request. 
  - If there is no such branch, it will create a branch and populate it with the latest baseline for that test from the repository name and target branch defined by the pull request. 
  - If there is no such baseline the test will be marked as "New".
- Eyes sends Bitbucket status information that is displayed on the Bitbucket Pull requests panel. Similar information is also displayed in the Bitbucket Commit panel.

